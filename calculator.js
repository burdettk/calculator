$(document).ready(function() {
  $('#calc').on('input', function(event) {              // making the numbers output appear on input rather than using a submit button
	event.preventDefault();
  var employees = document.getElementById('employees').value;         //variables to declare for user to input on client side html
  var salary = document.getElementById('salary').value;
  var absence = document.getElementById('absence').value;
  var month = 0;
  var year = 0;
  var payment = employees * salary;
  // finding the percentage of absence rate
  var percentage = absence / 100;
  // finding the yearly loss
  year = payment * percentage;                  // output of money on year
  //finding the monthly loss
  month = year / 12;
  document.getElementById('month').innerHTML = month.toFixed(2);      // fix the number to 2 decimal places
  document.getElementById('year').innerHTML =  year.toFixed(2);
  });
});
